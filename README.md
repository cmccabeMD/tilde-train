# tilde.train

General overview of tilde.train.

tilde.train is a variation on 'sl', the prank *NIX command that teaches you a lesson for accidentally typing
'sl' instead of 'ls' when trying to view the contents of a directory.  If you type 'sl', an animated steam
locomotive (hence the name 'sl') will cross your screen.

The tilde.train version of 'sl' will turn 'sl' into a multi-user novelty.  It will generate a new train each time it 
is run by scooping up user-created .choochoo files to represent the train cars.  This concept merges the multi-user 
input idea from tilde.town user troido's cadastre with the 'sl' idea.

The .choochoo files should be no more than X characters wide and Y characters tall (say 10x30).  Anything taller or 
wider than XxY should be ignored as bad input. -- or, if not Y chars high, should be verically padded with spaces to 
make Y height; and if at least one, but not all lines are X chars wide, then the non-X lines should be right-padded.

Options for tilde.train include:
-a print all existing cars to stdout (i.e. loop through all .choochoo files on the system)
-p print a train to stdout (e.g. for printing on a <pre></pre> html doc.
-t test your own train and get feedback on height, width or character validity
-h the help doc

A sample car might look like this:
    
     ----------------------------  
    |                            |
    |    YOUR TRAIN CAR HERE!    |  
    |         Just create a      |  
    |       ~/.choochoo file!    |
    |                            |
     - /  \-/  \------/  \-/  \ -
       \__/ \__/      \__/ \__/

The Engine Car:
    
            ____     
           |____|         ------------ 
            |  |  ===     | |------| | 
         ___|  |__| |_____| |  \O/ | | 
        |  |                |___V__| | 
       [[  |                         |
        |  | -------------|    t.t   |
         ____________________________| 
     //// / _\__/__\__/__\      /  \
    ////  \__/  \__/  \__/      \__/


The Caboose:
    
             =============
    =========|           |==========
      |    ----          ----     |
      |   |    |        |    |    |  
      |    ----          ----     |  
      |    tilde.town railways    |
    ==|                           |==
    == - /  \-/  \-----/  \-/  \ - ==
         \__/ \__/     \__/ \__/


And ideally, each car/engine/caboose should have some surface at both the left and right ends about 3 characters
up from the bottom, so that a connection link can be added between them.

Connection link:
    : (just the colon)
